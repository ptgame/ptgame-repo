@extends('adminlte::page')

@section('title', 'Modifica Città')

@section('content_header')
    <h2 class="my-3">Creazione Città</h2>
@stop

@section('content')


    @if($errors->all())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>
    @endif

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{session()->get('message')}}
        </div>
    @endif


    <form action="{{route('cities.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="citta">Nome Città</label>
            <input type="text" name="citta" id="citta" class="form-control">
        </div>


        <div class="form-group">
            <label for="zipcode">Codice Postale</label>
            <input type="text" name="zipcode" id="zipcode" class="form-control">
        </div>


        <div class="form-group">
            <label for="provincia">Provincia Città</label>
            <input type="text" name="provincia" id="provincia" class="form-control">
        </div>

        <div class="form-group">
            <label for="regione">Regione</label>
            <input type="text" name="regione" id="regione" class="form-control">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Aggiungi Città</button>
        </div>
    </form>
@endsection
