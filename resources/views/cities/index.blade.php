@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Lista Città <a href= "{{ route('cities.create') }}" class="btn btn-success btn-lg">Create</a> </h1>
@stop

@section('content')
    <div class="container">
        @foreach($cities as $city)
            <div class="row">
                <div class="col-sm-6">
                    {{$city->cityname}}
                </div>
                <div class="col-sm-6">
                    <form onsubmit="" class="col-md-2 form-inline" method="GET" action="{{ route('cities.edit', $city->id) }}">
                        @csrf
                        @method('get')
                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-edit"></span> EDIT
                        </button>
                    </form>

                    <form onsubmit="return confirm('Vuoi Cancellare?')" class="col-md-2 form-inline" method="POST" action="{{ route('cities.destroy', $city->id) }}">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">
                            <span class="glyphicon glyphicon-trash"></span> DELETE
                        </button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
    {{$cities->links()}}
@stop


