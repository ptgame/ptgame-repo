@extends('adminlte::page')

@section('title', 'Modifica Città')

@section('content_header')
    <h2 class="my-3">Modificare Città</h2>
@stop

@section('content')


    @if($errors->all())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>
    @endif

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{session()->get('message')}}
        </div>
    @endif

    <form action="{{route('cities.update', $city->id)}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="citta">Nome Città</label>
            <input type="text" name="citta" id="citta" class="form-control" value='{{$city->cityname}}'>
        </div>


        <div class="form-group">
            <label for="zipcode">Codice Postale</label>
            <input type="text" name="zipcode" id="zipcode" class="form-control" value='{{$city->zipcode}}'>
        </div>


        <div class="form-group">
            <label for="provincia">Provincia Città</label>
            <input type="text" name="provincia" id="provincia" class="form-control" value='{{$city->provincia}}'>
        </div>

        <div class="form-group">
            <label for="regione">Regione</label>
            <input type="text" name="regione" id="regione" class="form-control" value='{{$city->regione}}'>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Aggiorna Città</button>
        </div>
    </form>
@endsection
