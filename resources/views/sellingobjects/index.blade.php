@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Lista Oggetti <a href= "{{ route('sellingobjects.create') }}" class="btn btn-success btn-lg">Create</a> </h1>
@stop

@section('content')
    <div class="container">
        @foreach($sellingobjects as $sellingobject)
            <div class="row">
                <div class="col-sm-6">
                    {{$sellingobjects->objectsname}}
                </div>
                <div class="col-sm-6">
                    <form onsubmit="" class="col-md-2 form-inline" method="GET" action="{{ route('sellingobjects.edit', $sellingobjects->id) }}">
                        @csrf
                        @method('get')
                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-edit"></span> EDIT
                        </button>
                    </form>

                    <form onsubmit="return confirm('Vuoi Cancellare?')" class="col-md-2 form-inline" method="POST" action="{{ route('sellingobjects.destroy', $sellingobjects->id) }}">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">
                            <span class="glyphicon glyphicon-trash"></span> DELETE
                        </button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
    {{$sellingobjects->links()}}
@stop