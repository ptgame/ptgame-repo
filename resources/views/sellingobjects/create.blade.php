@extends('adminlte::page')

@section('title', 'Modifica Oggetto')

@section('content_header')
    <h2 class="my-3">Creazione Oggetto</h2>
@stop

@section('content')


    @if($errors->all())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>
    @endif

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{session()->get('message')}}
        </div>
    @endif


    <form action="{{route('sellingobjects.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="nameobjects">Nome Oggetto</label>
            <input type="text" name="nameobjects" id="nameobject" class="form-control">
        </div>


        <div class="form-group">
            <label for="numberobject">Numero Oggetto</label>
            <input type="text" name="numberobject" id="numberobject" class="form-control">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Aggiungi Oggetto</button>
        </div>
    </form>
@endsection
