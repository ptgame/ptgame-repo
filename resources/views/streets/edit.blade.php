@extends('adminlte::page')

@section('title', 'Modifica Città')

@section('content_header')
    <h2 class="my-3">Modificare Città</h2>
@stop

@section('content')


    @if($errors->all())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>
    @endif

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{session()->get('message')}}
        </div>
    @endif

    <form action="{{route('streets.update', $street->id)}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="city">Select list:</label>
            <select class="form-control" id="city" name="city">
                @foreach($cities as $city)
                    <option @if($city->id == $street->city->id) selected="selected" @endif value="{{$city->id}}">{{$city->cityname}}</option>
                @endforeach
            </select>
        </div>


        <div class="form-group">
            <label for="via">Via</label>
            <input type="text" name="streetname" id="via" class="form-control" value='{{$street->streetname}}'>
        </div>


        <div class="form-group">
            <label for="provincia">Provincia Città</label>
            <input type="text" name="streetnumber" id="numerocivico" class="form-control" value='{{$street->streetnumber}}'>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Aggiorna Via</button>
        </div>
    </form>
@endsection
