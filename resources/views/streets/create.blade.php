@extends('adminlte::page')

@section('title', 'Modifica Città')

@section('content_header')
    <h2 class="my-3">Creazione Via</h2>
@stop

@section('content')


    @if($errors->all())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>
    @endif

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{session()->get('message')}}
        </div>
    @endif


    <form action="{{route('streets.store')}}" method="post">
        @csrf
         <div class="form-group">
            <label for="city">Select list:</label>
            <select class="form-control" id="city" name="city">
                @foreach($cities as $city)
                <option value="{{$city->id}}">{{$city->cityname}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="streetname">Via o Piazza</label>
            <input type="text" name="streetname" id="streetname" class="form-control">
        </div>

        <div class="form-group">
            <label for="streetnumber">Numero Civico</label>
            <input type="text" name="streetnumber" id="streetnumber" class="form-control">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Aggiungi Via</button>
        </div>
    </form>
@endsection
