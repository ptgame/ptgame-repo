@extends('adminlte::page')

@section('title', 'Lista Strade')

@section('content_header')
    <h1>Lista Strade  <a href= "{{ route('streets.create') }}" class="btn btn-success btn-lg">Create</a> </h1>


@stop

@section('content')
    <div class="container">
        @foreach($streets as $street)
            <div class="row">
                <div class="col-sm-6">
                {{$street->city->cityname}} - {{$street->streetname}}, {{$street->streetnumber}}
                </div>
                <div class="col-sm-6">
                    <form onsubmit="" class="col-md-2 form-inline" method="GET" action="{{ route('streets.edit', $street->id) }}">
                        @csrf
                        @method('get')
                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-edit"></span> EDIT
                        </button>
                    </form>

                    <form onsubmit="return confirm('Vuoi Cancellare?')" class="col-md-2 form-inline" method="POST" action="{{ route('streets.destroy', $street->id) }}">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">
                            <span class="glyphicon glyphicon-trash"></span> DELETE
                        </button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
@stop


