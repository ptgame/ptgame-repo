<?php

//Route::get('/', function () {
//    return view('welcome');
// });




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/importer', 'CityController@importer')->name('importer');

Route::get('/admin', 'AdminController@index')->name('admin');

Route::resource('cities', 'CityController');

Route::resource('streets', 'StreetController');

Route::resource('sellingobjects', 'SellingObjectsController');

Route::get('/', 'HomeController@index')->name('home');


