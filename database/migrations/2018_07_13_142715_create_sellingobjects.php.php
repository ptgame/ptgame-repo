<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellingObjects extends Migration
{
    public function up()
    {
        Schema::create('sellingobjects', function ($table) {
            $table->increments('id');
            $table->string('objectsname');
            $table->string('objectsnumber');
            $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('sellingobjects');
    }
}
