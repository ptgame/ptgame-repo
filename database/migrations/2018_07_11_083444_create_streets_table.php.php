<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreetsTable extends Migration
{
    public function up()
    {
        Schema::create('streets', function ($table) {
            $table->increments('id');
            $table->string('streetname');
            $table->string('streetnumber');
            $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('streets');
    }
}
