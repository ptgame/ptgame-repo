<?php

use Illuminate\Database\Seeder;
use Illuminate\Contracts\Redis\Factory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
            UsersTableSeeder::class,
            RolesAndPermissionsSeeder::class,
        ]);

    }
}
