<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        DB::table('users')->insert([
            'name' => 'Noemi',
            'email' => 'noemibrancalion@gmail.com',
            'password' => bcrypt('userpass'),
        ]);
        DB::table('users')->insert([
            'name' => 'Andrea',
            'email' => 'petrelli.andrea200@gmail.com',
            'password' => bcrypt('userpass'),
        ]);
        DB::table('users')->insert([
            'name' => 'Luca',
            'email' => 'frangio.luca@gmail.com',
            'password' => bcrypt('userpass'),
        ]);
        DB::table('users')->insert([
            'name' => 'Massimo',
            'email' => 'massimo.zaglio@top-ix.org',
            'password' => bcrypt('userpass'),
        ]);

    }
}
