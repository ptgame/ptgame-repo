<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SellingObjects extends Model
{
    protected  $table= 'sellingobjects';
    public $timestamps = true;
}
