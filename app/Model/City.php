<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected  $table= 'cities';
    public $timestamps = true;

    public function streets()
    {
        return $this->hasMany('App\Model\Street');
    }
}
