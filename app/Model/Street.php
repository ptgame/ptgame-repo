<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    protected  $table= 'streets';
    public $timestamps = true;

    public function city()
    {
        return $this->belongsTo('App\Model\City');
    }
}
