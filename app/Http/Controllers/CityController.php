<?php

namespace App\Http\Controllers;

use App\Model\City;
use Illuminate\Http\Request;

class CityController extends Controller
{

    public function index()
    {
        $cities = City::where('delete',0)->paginate(200);
        return view('cities.index',['cities' => $cities]);
    }

    public function create()
    {
        return view('cities.create');

    }

    public function store(Request $request)
    {
        $city = new City();
        $city->cityname = $request->citta;
        $city->zipcode = $request->zipcode;
        $city->provincia = $request->provincia;
        $city->regione = $request->regione;
        $city->save();
        return redirect()->route('cities.index');

    }

    public function show($id)
    {


    }


    public function edit($id)
    {
        $city = City::where('id',$id)->first();
        return view('cities.edit',['city' => $city]);

    }

    public function update(Request $request, $id)
    {
        $city = City::where('id',$id)->first();
        $city->cityname = $request->citta;
        $city->zipcode = $request->zipcode;
        $city->provincia = $request->provincia;
        $city->regione = $request->regione;
        $city->save();
        return redirect()->route('cities.index');

    }

    public function destroy($id)
    {
        $city = City::where('id',$id)->first();
        $city->delete = 1;
        $city->save();
        return redirect()->route('cities.index');
    }

    public function importer()
    {

        if (($handle = fopen(public_path() . '/cap.csv', 'r')) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ';')) !== FALSE) {


                $csv_data = new City();
                $csv_data->cityname = $data [0];
                $csv_data->zipcode = $data [1];
                $csv_data->provincia = $data [2];
                $csv_data->regione = $data [3];
                $csv_data->save();
            }
        }
        fclose($handle);

        $finalData = $csv_data::all();

        return view('importer', [
            "data" => $finalData,
        ]);
    }
 }

