<?php

namespace App\Http\Controllers;

use App\Model\SellingObjects;
use Illuminate\Http\Request;


class SellingObjectsController extends Controller
{

    public function index()
    {

        $sellingobjects = SellingObjects::where('delete',0)->paginate(50);
        return view('sellingobjects.index',['sellingobjects' => $sellingobjects]);
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
