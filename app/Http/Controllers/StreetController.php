<?php


namespace App\Http\Controllers;
use App\Model\City;
use App\Model\Street;
use Illuminate\Http\Request;

class StreetController extends Controller
{

    public function index()
    {
        $streets = Street::where('delete',0)->paginate(50);
        return view('streets.index',['streets' => $streets]);
    }

    public function create()
    {
        $cities = City::where('delete',0)->get();
        return view('streets.create',['cities' => $cities]);
    }


    public function store(Request $request)
    {

        $streets = new Street();

        $streets->city_id = $request->city;

        $streets->streetname = $request->streetname;

        $streets->streetnumber = $request->streetnumber;

        // dd($request);
        $streets->save();



        return redirect()->route('streets.index');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $street = Street::where('id',$id)->first();
        $cities = City::where('delete',0)->get();

        return view('streets.edit',[  'street' => $street,
                                            'cities' => $cities]);
    }

    public function update(Request $request, $id)
    {
        $streets = Street::where('id',$id)->first();
        $streets->city_id = $request->city;
        $streets->streetname = $request->streetname;
        $streets->streetnumber = $request->streetnumber;
        $streets->save();
        return redirect()->route('streets.index');


    }


    public function destroy($id)
    {
        $streets = Street::where('id',$id)->first();
        $streets->delete =1;
        $streets->save();
        return redirect()->route('streets.index');
    }
}
